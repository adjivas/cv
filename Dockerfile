FROM debian

RUN apt-get update -qq
RUN apt-get install -y locales

RUN echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen

RUN locale-gen
RUN update-locale LANG=fr_FR.UTF-8

ENV LANG=fr_FR.UTF-8
ENV LC_ALL=fr_FR.UTF-8 

RUN apt-get install -y build-essential \
                       latex-make \
                       texlive-luatex \
                       texlive-base \
                       texlive-xetex \
                       texlive-latex-recommended \
                       texlive-latex-extra \
                       texlive-extra-utils \
                       texlive-fonts-recommended \
                       texlive-font-utils \
                       texlive-lang-french \
                       texlive-pictures \
                       texlive-bibtex-extra \
                       lmodern \
                       fonts-lmodern \
                       biber \
                       latexmk \
                       ghostscript \
                       hevea \
                       python3 \
                       vim


ENV APP_HOME /app

RUN mkdir $APP_HOME
ADD . $APP_HOME
WORKDIR $APP_HOME
RUN make -C .
# ADD ./images $APP_HOME/cv/images

EXPOSE 8000

CMD python3 -m http.server --directory $APP_HOME/cv 8000
