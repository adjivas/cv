PDFLATEX=lualatex

HEVEA=hevea
HEVEAOPTS=-exec xxdate.exe -fix
HACHA=hacha
IMAGEN=imagen

#document base name
DOC=cv
DIR=$(DOC)
BASE=$(DIR)/$(DOC)
INSTALLDIR=$(HOME)/public_html/$(DOC)

all: $(DOC) $(BASE).html $(BASE).pdf

$(DOC):
	mkdir -p $(DOC)

$(BASE).pdf: $(DOC).tex
	$(PDFLATEX) --output-dir=$(DIR) $(DOC).tex $(DOC).pdf

$(DIR)/index.html: $(BASE).html
	$(HACHA) -tocter -o $(DIR)/index.html $(BASE).html

$(BASE).html: $(DOC).hva $(DOC).tex
	$(HEVEA) $(HEVEAOPTS) $(DOC).hva -o $(BASE).html $(DOC).tex

install: partialclean
	cp $(DIR)/* $(INSTALLDIR)

partialclean:
	rm -f $(BASE).h{tml,aux,toc,ind} $(BASE).image.*

clean:
	rm -f $(BASE).pdf
	rm -f $(BASE).html $(BASE)[0-9][0-9][0-9].html $(BASE).haux
	rm -f $(BASE).htoc
	rm -f $(BASE).image.*
	rm -f $(BASE)[0-9][0-9][0-9].gif
